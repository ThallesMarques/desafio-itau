package br.com.itau.modelo;

import java.util.List;

import br.com.itau.service.LancamentoService;

public class Exercicio {

	public static void main(String[] args) {	
		List<Lancamento> lancamentosMes = new LancamentoService().listarGastosOrdenadosPorMeses();
        List<Lancamento> categorias = new LancamentoService().mostrarLancamentosPorCategoria();
        Double totalFatura = new LancamentoService().mostrarTotalFaturaPorMes();
        
        System.out.println("Gastos ordenados por meses: ");

        for (Lancamento lanca : lancamentosMes) {
            double valor = lanca.getValor();
            int mes = lanca.getMes();
            System.out.println("mes:" + mes +"-" +  "Valor: " + valor);
        }
        System.out.println("Todos os lancamentos da categoria 6: " + categorias);
        System.out.println("Total fatura mes 7: " + totalFatura);
	}
}